const fs = require("fs");
const path = require("path");

const FilterWarningsPlugin = require("webpack-filter-warnings-plugin");

// const withImage = require("./configs/next-image.config");
// const withDotenv = require("./configs/next-dotenv.config");
const withAntd = require("./next-antd.config");

// fix: prevents error when .less files are required by node
if (typeof require !== "undefined") {
    require.extensions[".less"] = file => {};
}

module.exports = withAntd({
    cssModules: true,
    cssLoaderOptions: {
        sourceMap: false,
        importLoaders: 1
    },
    lessLoaderOptions: {
        javascriptEnabled: true
    },
    webpack: config => {
        config.plugins.push(
            new FilterWarningsPlugin({
                // ignore ANTD chunk styles [mini-css-extract-plugin] warning
                exclude: /mini-css-extract-plugin[^]*Conflicting order between:/
            })
        );

        return config;
    }
});
